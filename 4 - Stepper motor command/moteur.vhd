library ieee;
use ieee.std_logic_1164.all;

entity moteur is
	port(
		clk: in std_logic;
		Rot, OnOff: in std_logic;
		phase: out std_logic_vector(3 downto 0)
	);
end moteur;

architecture rtl of moteur is
	type state_type is (s0, s1, s2, s3, s4);
	signal state: state_type;
begin
	process (clk, OnOff)
	begin
		if rising_edge(clk) then
			case(state) is
				when s0 => --idle
					if OnOff = '1' then state <= s0;
					else state <= s1; end if;
				when s1=> --b0
					if OnOff = '1' then state <= s0;
					else state <= s2; end if;
				when s2 => --b1
					if OnOff = '1' then state <= s0;
					else state <= s3;end if;
				when s3 => --b2
					if OnOff = '1' then state <= s0;
					else state <= s4; end if;
				when s4 => --b3
					if OnOff = '1' then state <= s0;
					else state <= s1; end if;
				when others => state <= s0;
			end case;
		end if;
	end process;

	process (state, Rot)
	begin
		case(state) is
			when s0 =>
				phase <= "0000";
			when s1 =>
				if Rot = '1' then phase <= "1001";
				else phase <= "0011"; end if;
			when s2 =>
				if Rot = '1' then phase <= "1100";
				else phase <= "0110"; end if;
			when s3 =>
				if Rot = '1' then phase <= "0110";
				else phase <= "1100"; end if;
			when s4 =>
				if Rot = '1' then phase <= "0011";
				else phase <= "1001"; end if;
			when others => phase <= "----";
		end case;
	end process;
end rtl;
