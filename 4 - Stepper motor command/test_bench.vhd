library ieee;
use ieee.std_logic_1164.all;

entity tb is
end tb;

architecture a of tb is
	component top is
		generic (
			div: integer := 250000 -- 200Hz with a 50MHz clk
		);
		port (
			clk: in std_logic;
			rotation, power: in std_logic;
			phase: out std_logic_vector (3 downto 0)
		);
	end component;

	constant div: integer := 250000;
	signal clk: std_logic := '1';
	signal rotation: std_logic := '1';
	signal power: std_logic := '0';
	signal phase: std_logic_vector (3 downto 0);
begin
	top_inst: top
	generic map (
		div => div
	)
	port map (
		clk => clk,
		rotation => rotation,
		power => power,
		phase => phase
	);

	process
	begin
		wait for 20 ns;
		clk <= not clk;
	end process;
	process
	begin
		wait for 4 us;
		power <= '1';
		wait for 2 us;
		power <= '0';
		rotation <= '0';	
		wait;
	end process;
end a;
