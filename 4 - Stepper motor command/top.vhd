library ieee;
use ieee.std_logic_1164.all;

entity top is
	generic (
		div: integer := 250000 -- 200Hz with a 50MHz clk
	);
	port (
		clk: in std_logic;
		rotation, power: in std_logic;
		phase: out std_logic_vector (3 downto 0)
	);
end top;

architecture struct of top is
	signal clkdiv: std_logic;

	component moteur is
		port (
			clk: in std_logic;
			Rot, OnOff: in std_logic;
			phase: out std_logic_vector (3 downto 0)
		);
	end component;
	
	component divider is
		generic (
			N: integer
		);
		port (
			clk: in std_logic;
			cout: out std_logic := '0'
		);	
	end component;
begin
	divider_inst: divider
	generic map (
		N => div
	)
	port map (
		clk => clk,
		cout => clkdiv
	);

	moteur_inst: moteur
	port map (
		clk => clkdiv,
		Rot => rotation,
		OnOff => power,
		phase => phase
	);
end struct;
