width = 8;
depth = 1024;

t = linspace(0, 2*pi, depth);

f = fopen("sinus.mif", 'w');
fprintf(f, "WIDTH=%d;\n", width);
fprintf(f, "DEPTH=%d;\n\n", depth);
fprintf(f, "ADDRESS_RADIX=UNS;\n");
fprintf(f, "DATA_RADIX=BIN;\n\n");
fprintf(f, "CONTENT BEGIN\n");

Q = sin(t);

for ii = 1:depth
	if Q(ii) < 0 % unsigned: [-2^(n-1), 2^(n-1)-1], signed: [0 2^(n)-1]
		q = dec2bin(2^(width-1) + round(Q(ii) * 2^(width-1) / max(Q)), width);
	else
		q = dec2bin(2^(width-1) + round(Q(ii) * (2^(width-1) - 1) / max(Q)), width);
	end

	fprintf(f, "\t%i : %s;\n", ii-1, num2str(q));
end

fprintf(f, "END;");
fclose(f);
