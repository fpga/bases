library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tb is
end tb;

architecture a of tb is
	component dds is
		generic (
			WDTH: integer := 8; 
			N_W: integer := 10; 
			N_O: integer := 6 
		);
		port (
			clk: in std_logic;
			freq_word: in std_logic_vector (N_W-1 downto 0);
			offset: in std_logic_vector (N_O-1 downto 0);
			sig_out: out std_logic_vector (WDTH-1 downto 0)
			 );
	end component;

	signal clk: std_logic;
	signal word: std_logic_vector (9 downto 0);
	signal offset: std_logic_vector (5 downto 0);
	signal binout: std_logic_vector (7 downto 0);
begin
	top: dds
	generic map (
		WDTH => 8, N_W => 10, N_O => 6
	)
	port map (
		clk => clk,
		freq_word => word, offset => offset,
		sig_out => binout
	);

	process
	begin
		clk <= '0'; wait for 10 ns; 
		clk <= '1'; wait for 10 ns; 
	end process;

	process
	begin
		word <= "0000001000";
		offset <= "000000";
		wait for 10 us;
		word <= "0010000000";
		offset <= "001000";
		wait;
	end process;
end a;
