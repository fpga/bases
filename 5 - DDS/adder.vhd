library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity adder is
	generic(
		N: integer -- input bit length
		);
	port(
		clk: in std_logic;
		a, b: in std_logic_vector (N-1 downto 0);
		s: out std_logic_vector (N-1 downto 0)
	);
end adder;

architecture rtl of adder is
begin
	process(clk, a, b)
	begin
		if rising_edge(clk) then
			s <= std_logic_vector(unsigned(a) + unsigned(b)); -- we don't care about the carry: we cycle back
		end if; -- else: simple memory latch
	end process;
end rtl;
