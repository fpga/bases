library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity dds is
	generic (
		WDTH: integer := 8; -- digital output
		N_W: integer := 10; -- len of the input word
		N_O: integer := 6 -- len of the input phase offset
	);
	port (
		clk: in std_logic;
		freq_word: in std_logic_vector (N_W-1 downto 0);
		offset: in std_logic_vector (N_O-1 downto 0);
		sig_out: out std_logic_vector (WDTH-1 downto 0)
	     );
end dds;

architecture structural of dds is
	signal phase: std_logic_vector (N_W-1 downto 0) := (others => '0');
	signal freq: std_logic_vector (N_W-1 downto 0);
	
	component adder is
		generic(
			N: integer
			);
		port(
			clk: in std_logic;
			a, b: in std_logic_vector (N-1 downto 0);
			s: out std_logic_vector (N-1 downto 0)
		);
	end component;

	component ram is
		generic (
			W: integer;
			D: integer
		);
		port (
			clk: in std_logic;
			addr: in std_logic_vector (D-1 downto 0);
			q: out std_logic_vector (W-1 downto 0)
		);
	end component;

	-- size stuff, could have been hardcoded to avoid that
	constant concat: std_logic_vector (N_W-N_O-1 downto 0) := (others => '0');
	signal offset_N_W_sized: std_logic_vector (N_W-1 downto 0);
	-- we suppose N_W > N_O for all cases
begin
	
	phase_adder: adder
	generic map (N => N_W)
	port map (
		clk => clk,
		a => freq_word, b => phase,
		s => phase
	);

	offset_adder: adder
	generic map (N => N_W)
	port map (
		clk => clk,
		a => offset_N_W_sized, b => phase,
		s => freq
	);

	offset_N_W_sized <= concat & offset;

	ram_inst: ram
	generic map (
		W => WDTH,
		D => N_W
	)
	port map (
		clk => clk,
		addr => freq, -- same size tanks to depth
		q => sig_out
	);

end structural;
