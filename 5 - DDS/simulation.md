## DDS simulation

With ModelSim Altera, TCL script without test bench:

``` tcl
restart -force -nobreakpoint -nofcovers -nolog
force clk 0 0, 1 10ns -repeat 20ns #50MHz
force f_word 0110000000
force o_word 0000100000
run 20us
```

Execute with `Tools > TCL > Execute Macro`.<br>

