library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ram is -- a ram without input data nor write ability
	generic (
		W: integer; -- output width
		D: integer  -- bit-width of the nb of addresses
	);
	port (
		clk: in std_logic;
		addr: in std_logic_vector (D-1 downto 0);
		q: out std_logic_vector (W-1 downto 0)
	);
end ram;

architecture rtl of ram is
	-- RAM depth
	constant depth_v: std_logic_vector (D-1 downto 0) := (others => '1');
	constant depth: integer := to_integer(unsigned(depth_v)); -- should be 2^D - 1

	type mem is array (0 to depth) of std_logic_vector (W-1 downto 0);
	signal ramval: mem;
	attribute ramval_init_file: string;
	attribute ramval_init_file of ramval: signal is "sinus.mif";
	-- Quartus VHDL RAM synthesis
	-- https://www.intel.com/content/www/us/en/programmable/quartushelp/17.0/hdl/vhdl/vhdl_file_dir_ram_init.htm
begin
	process (clk, addr)
	begin
		if rising_edge(clk) then
			q <= ramval(to_integer(unsigned(addr)));
		end if;
	end process;
end rtl;
