library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity alu is
port(
	A, B: in std_logic_vector (2 downto 0);
	sel: in std_logic_vector (1 downto 0);
	alu_out: out std_logic_vector (3 downto 0)
	);
end alu;

architecture behavioral of alu is
	begin
	process(sel, A, B)
	begin
		case(sel) is
			when "00" => alu_out <= ('0' & (A and B));
			when "01" => alu_out <= ('0' & (A or B));
			when "10" => alu_out <= std_logic_vector(unsigned(('0' & A)) + unsigned(('0' & B)));
			when "11" => alu_out <= (A & '0');
			when others => alu_out <= ('0' & (A and B));
		end case;
	end process;
end behavioral;
			
