library ieee;
use ieee.std_logic_1164.all;

entity tb is
end tb;

architecture a of tb is
	component top is
		port (
			A, B: in std_logic_vector (2 downto 0);
			opcode: in std_logic_vector (1 downto 0);
			segments: out std_logic_vector (6 downto 0)
		);
	end component;

signal segments: std_logic_vector (6 downto 0);
signal opcode: std_logic_vector (1 downto 0);
signal A, B: std_logic_vector (2 downto 0);

begin
	top_inst: top
	port map (
		A => A,
		B => B,
		opcode => opcode,
		segments => segments
	);

	process
	begin
		A <= "111";
		B <= "110";
		
		opcode <= "00";
		wait for 1 ns;
		opcode <= "01";
		wait for 1 ns;
		opcode <= "10";
		wait for 1 ns;
		opcode <= "11";
		wait for 1 ns;

		wait;
	end process;
end a;
