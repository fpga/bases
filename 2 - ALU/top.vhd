library ieee;
use ieee.std_logic_1164.all;

entity top is
	port (
		A, B: in std_logic_vector (2 downto 0);
		opcode: in std_logic_vector (1 downto 0);
		segments: out std_logic_vector (6 downto 0)
	);
end top;

architecture struct of top is
	signal result: std_logic_vector (3 downto 0);

	component decodeur is
		port (
			entry: in std_logic_vector (3 downto 0);
			s: out std_logic_vector (6 downto 0)
		);
	end component;
	
	component alu is
		port (
			A, B: in std_logic_vector (2 downto 0);
			sel: in std_logic_vector (1 downto 0);
			alu_out: out std_logic_vector (3 downto 0)
		);
	end component;
begin
	alu_inst: alu
	port map (
		A => A,
		B => B,
		sel => opcode,
		alu_out => result
	);

	decodeur_inst: decodeur
	port map (
		entry => result,
		s => segments
	);
end struct;
