library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity pipe is
    generic (
        W_adder: integer := 8;
        N_adder: integer := 4;
        W_input: integer := 32 -- W_adder * N_adder
    );
    port (
        clk: in std_logic;
        i_1, i_2: in std_logic_vector (W_input-1 downto 0);
        o_sum: out std_logic_vector (W_input-1 downto 0) := (others => '0')
    );
end pipe;

architecture struct of pipe is
    component adder is
        generic(
            N: integer -- input bit length
            );
        port(
            clk: in std_logic;
            a, b: in std_logic_vector (N-1 downto 0);
            cin: in std_logic;
            s: out std_logic_vector (N-1 downto 0);
            cout: out std_logic
        );
    end component;
    component dff is
        generic (
            LEN: integer
        );
        port (
            clk: in std_logic;
            D: in std_logic_vector (LEN-1 downto 0);
            Q: out std_logic_vector (LEN-1 downto 0)
        );
    end component;

    signal carry: std_logic_vector (N_adder downto 0) := (others => '0');
    type sig_tmp is array (0 to 2, 0 to N_adder-1, 0 to N_adder-1) of std_logic_vector (W_adder-1 downto 0);
    signal wire: sig_tmp := (others => (others => (others => (others => '0'))));
    -- dim1={A,B,Out}, dim2={adder}, dim3{dff}, filled with dim4{data}
begin
    global_gen:
    for j in 0 to N_adder-1 generate
        wire(0, j, 0) <= i_1(W_adder*(j+1)-1 downto W_adder*j); -- slicing the inputs
        wire(1, j, 0) <= i_2(W_adder*(j+1)-1 downto W_adder*j);
        o_sum(W_adder*(j+1)-1 downto W_adder*j) <= wire(2, j, N_adder-1); -- concatenation for the output

        gen_double_dffs: -- transmitting inputs a and b to the j-th adder
        for kd in 1 to j generate
            uud00: dff
            generic map (LEN => W_adder)
            port map (clk => clk, D => wire(0, j, kd-1), Q => wire(0, j, kd));
            uud01: dff
            generic map (LEN => W_adder)
            port map (clk => clk, D => wire(1, j, kd-1), Q => wire(1, j, kd));
        end generate;

        gen_dffs: -- transmitting output s of the j-th adder to sync with the other adder's output
        for k in j+1 to N_adder-1 generate
            uud1: dff
            generic map (LEN => W_adder)
            port map (clk => clk, D => wire(2, j, k-1), Q => wire(2, j, k));
        end generate;

        adder_inst: adder
        generic map (N => W_adder)
        port map (
            clk => clk,
            a => wire(0, j, j),
            b => wire(1, j, j),
            s => wire(2, j, j),
            cin => carry(j),
            cout => carry(j+1)
        );
    end generate;
carry(0) <= '0';
end struct;
