library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity adder is
    generic (
        N: integer -- width of inputs
    );
	port(
		clk: in std_logic;
		a, b: in std_logic_vector (N-1 downto 0);
        cin: in std_logic;
		s: out std_logic_vector (N-1 downto 0);
        cout: out std_logic
	);
end adder;

architecture rtl of adder is
    signal operation: std_logic_vector (N downto 0); -- MSB for the carry
    signal carry_handle: std_logic_vector (N-1 downto 0) := (others => '0'); -- for concatenation of cin
begin
    process(clk, a, b)
    begin
        if rising_edge(clk) then
            operation <= std_logic_vector( unsigned('0' & a) + unsigned('0' & b) + unsigned (carry_handle & cin));
        end if;
    end process;
cout <= operation(N);
s <= operation(N-1 downto 0);
end rtl;
