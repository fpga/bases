library ieee;
use ieee.std_logic_1164.all;

entity dff is
    generic (
        LEN: integer
    );
    port (
        clk: in std_logic;
        D: in std_logic_vector (LEN-1 downto 0);
        Q: out std_logic_vector (LEN-1 downto 0)
    );
end dff;

architecture rtl of dff is
begin
    process(clk, D)
    begin
        if rising_edge(clk) then Q <= D; end if;
    end process;
end rtl;
