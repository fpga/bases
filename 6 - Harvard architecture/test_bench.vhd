library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tb is
end tb;

architecture a of tb is
	component pipe is
        generic (
            W_adder: integer := 8;
            N_adder: integer := 4;
            W_input: integer := 32
        );
        port (
            clk: in std_logic;
            i_1, i_2: in std_logic_vector (W_input-1 downto 0);
            o_sum: out std_logic_vector (W_input-1 downto 0)
        );
	end component;

	signal clk: std_logic;
    signal A, B: std_logic_vector (31 downto 0) := (others => '0');
    signal output: std_logic_vector (31 downto 0);
begin
	top: pipe
	generic map (
		W_adder => 8, N_adder => 4, W_input => 32
	)
	port map (
		clk => clk,
		i_1 => A, i_2 => B,
		o_sum => output
	);

	process
	begin
		clk <= '1'; wait for 10 ns; 
		clk <= '0'; wait for 10 ns; 
	end process;

	process
	begin
        A <= "00000000000000000000000000000000";
        B <= "11111111111111111111111111111111";
        wait for 20 ns;
        B <= "00000000000000000000000000000000";
        A <= "11111111111111000000000000000000";
        wait for 20 ns;
        A <= "00000000000000000000000000000000";
        B <= "11111111111111111111111111111111";
        wait for 20 ns;
        B <= "11111111111111000000000000000000";
        A <= "11111111111111111111111111111111";
        wait for 20 ns;
        A <= "00000000000000000000000000000000";
        B <= "11111111111111111111111111111111";
        wait for 20 ns;
        B <= "00000000000000000000000000000000";
        A <= "11111111111111111111111111111111";
        wait for 20 ns;
        A <= "00000000000000000000000000000000";
        B <= "11111111111111111111111111111111";
        wait for 20 ns;
        B <= "00000000000000000000000000000000";
        A <= "11111111111111111111111111111111";
        wait;
	end process;
end a;
