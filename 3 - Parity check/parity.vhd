library ieee;
use ieee.std_logic_1164.all;

entity parity is
	generic (
		N: integer := 8
	);
	port (
		inp: in std_logic_vector (N-1 downto 0);
		par: out std_logic
	);
end parity;

architecture gener_struct of parity is
	signal res: std_logic_vector (N downto 0);
begin

    even_start: if N mod 2 = 0 generate res(0) <= '0'; end generate;
    odd_start: if N mod 2 = 1 generate res(0) <= '1'; end generate;

	xorgen: for i in 0 to N-1 generate
		res(i+1) <= res(i) xor inp(i);
	end generate xorgen;

	par <= res(N);

end gener_struct;
		
		
