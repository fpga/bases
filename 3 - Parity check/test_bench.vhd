library ieee;
use ieee.std_logic_1164.all;

entity tb is
end tb;

architecture a of tb is

component parity is
	generic (
		N: integer := 8
	);
	port (
		inp: in std_logic_vector (N-1 downto 0);
		par: out std_logic
	);
end component;

signal len: integer := 8;
signal number: std_logic_vector (len-1 downto 0);
signal result: std_logic;

begin

	parity_inst: parity
	generic map (
		N => len
	)
	port map (
		inp => number,
		par => result
	);

	process
	begin
		number <= "10001110";
		wait for 1 ns;
		number <= "10011110";
		wait for 1 ns;
		number <= "01001000";
		wait for 1 ns;
		number <= "11101110";
		wait for 1 ns;
		number <= "11111111";
		wait for 1 ns;
		number <= "10001110";
		wait for 1 ns;
		number <= "11110010";
		wait for 1 ns;
		number <= "01010100";
		wait for 1 ns;
		number <= "10101010";
		wait for 1 ns;

		wait;
	end process;
end a;
