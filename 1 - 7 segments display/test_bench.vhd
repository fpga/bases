library ieee;
use ieee.std_logic_1164.all;

entity tb is
end tb;

architecture a of tb is
	component top is
		generic (
			N: integer := 50000000
		);
		port (
			clk50MHz: in std_logic;
			segments: out std_logic_vector (6 downto 0)
		);
	end component;

signal N: integer := 50;
signal clk: std_logic;
signal segments: std_logic_vector (6 downto 0);

begin
	top_inst: top
	generic map(
		N => N
	)
	port map(
		clk50MHz => clk,
		segments => segments
	);

	process
	begin
		clk <= '0'; wait for 10 ns; -- F=50MHz -> T=20ns
		clk <= '1'; wait for 10 ns; -- Here we use 1MHz to unweighted the simulation
	end process;

	process
	begin
		wait;
	end process;
end a;
