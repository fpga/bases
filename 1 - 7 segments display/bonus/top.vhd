library ieee;
use ieee.std_logic_1164.all;

entity top is
	generic (
		N: integer := 50000000
	);
	port (
		clk50MHz: in std_logic;
		segments_MSB: out std_logic_vector (6 downto 0);
		segments_LSB: out std_logic_vector (6 downto 0)
	);
end top;

architecture struct of top is
	signal clk1s: std_logic;
	signal count: std_logic_vector (7 downto 0);

	component divider is
		generic (
			N: integer := 50000000
		);
		port (
			clk: in std_logic;
			cout: out std_logic
		);
	end component;

	component counter is
		port (
			clk, en: in std_logic;
			s: out std_logic_vector (7 downto 0)
		);
	end component;

	component decodeur is
		port (
			entry: in std_logic_vector (3 downto 0);
			s: out std_logic_vector (6 downto 0)
		);
	end component;

begin
	div_inst: divider
	generic map (
		N => N
	)
	port map (
		clk => clk50MHz,
		cout => clk1s
	);

	counter_inst: counter
	port map (
		clk => clk50MHz,
		en => clk1s,
		s => count
	);

	MSBs: decodeur
	port map (
		entry => count(7 downto 4),
		s => segments_MSB
	);


	LSBs: decodeur
	port map (
		entry => count(3 downto 0),
		s => segments_LSB
	);
end struct;
