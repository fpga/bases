library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity counter is
	generic (
		Max: std_logic_vector (7 downto 0) := x"3B" -- 59
	);
	port (
		clk, en: in std_logic;
		s: out std_logic_vector (7 downto 0)
	);
end counter;

architecture rtl of counter is
	signal q: std_logic_vector (7 downto 0) := (others => '0');
begin
	process(clk)
	begin
		if rising_edge(clk) and en = '1' then
			if q >= Max then
				q <= (others => '0');
			else
				q <= std_logic_vector(unsigned(q) + 1);
			end if;
		end if;
	end process;
	s <= q;
end rtl;
