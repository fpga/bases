library ieee;
use ieee.std_logic_1164.all;

entity divider is
	generic (
		N: integer := 50000000 -- F = 50MHz
	);
	port (
		clk: in std_logic;
		cout: out std_logic := '0'
	);
end divider;

architecture rtl of divider is
	signal k: integer range 0 to 50000000 := 0;
begin
	process(clk)
	begin
		if rising_edge(clk) then
			if k >= N then
				cout <= '1';
				k <= 0;
			else
				cout <= '0';
				k <= k + 1;
			end if;
		end if;
	end process;
end rtl;
