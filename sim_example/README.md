# Example of a compilation and simulation with GHDL
Illustation of the use of a testbench over a DUT (here, a simple D-Flipflop).

 Installation [here](https://github.com/ghdl/ghdl/releases).
```bash
$ tar -xvf ghdl-x.x.x.tar.gz && cd ghdl-x.x.x
$ ./configure && make && sudo make
```

Waveform viewer : GTKwave
```bash
sudo apt-get install gtkwave
```

## Options for the Makefile
```make
STOP: time duration
GHW: name of the .ghw file
VCD: name of the .vcd file
WORK: work/build directory
```

Replace `TOPTB` and `TOPIDENTITY` by the appropirate names off your test-bench code, and fill `FILES` with the necessary code you want to simulate.
