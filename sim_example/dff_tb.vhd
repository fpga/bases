library ieee;
use ieee.std_logic_1164.all;

entity tb is
end tb;

architecture a of tb is
	
component dff is
port(
	clk, d: in std_logic;
	q: out std_logic
);
end component;

signal clk, D: std_logic;
signal Q: std_logic;

begin

	dff_inst: dff
	port map(
		clk => clk,
		d => D,
		q => Q
	);

	process
	begin
		clk <= '0'; wait for 1 ns;
		clk <= '1'; wait for 1 ns;
	end process;

	process
	begin
		D <= '1';	
		for i in 1 to 5 loop
			wait until clk = '1';
		end loop;
		D <= '0';
		wait;
	end process;
end a;
