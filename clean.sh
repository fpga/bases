#! /usr/bin/bash

for d in *; do
	[[ -e "$d" ]] || break
	if [[ "$d" != "sim_example" ]] && [[ "$d" != "clean.sh" ]]; then
		(
			cd "$d" || exit
			make clean
		)
	fi
done

(
	cd "1 - 7 segments display/bonus" || exit
	make clean
)
